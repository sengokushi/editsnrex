# EditSnrEX

"EditSnrEX" is a tool of the historical war simulation game "Sengokushi".

You can efficiently edit the Sengokushi scenario using spreadsheet software.

# DEMO

![](https://wiki.sengokushi.net/_media/%E6%88%A6%E5%9B%BD%E5%8F%B2editsnrex.png)


# Features

By using the tool, batch editing is possible rather than standard editor.
Therefore, the efficiency can be greatly improved.
In the future, we will make use of its goodness to implement better functions.

# Requirement

Basic Tool (EditSnrEX.xlsm)
  * Microsoft Office Excel 2007 or higher version

For LibreOffice Version (EditSnrEX_LO.ods)
  * LibreOffice 6.4

# Installation

Download to any folder.

# Usage

Open the file and enable macros.

# Note

We are looking for collaborators and useful advice to customize with us.

# Author

* Modified by (C) Shuken 2020
* Created by (C) 水銀 2010

# License

"EditSnrEX" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
